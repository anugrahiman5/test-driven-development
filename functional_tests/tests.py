from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import WebDriverException
import time
import unittest

MAX_WAIT = 10

class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.quit()

    def wait_for_row_in_list_table(self, row_text):
        start_time = time.time()
        while True:
            try:
                table = self.browser.find_element_by_id('id_list_table')
                rows = table.find_elements_by_tag_name('tr')
                self.assertIn(row_text, [row.text for row in rows])
                return
            except (AssertionError, WebDriverException) as e:
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.5)

    def check_for_comment(self, teks):
        comment = self.browser.find_element_by_id('comment')
        self.assertEqual(teks, comment.text)

    def input_something_to_list(self, number, text):
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys(text)

        inputbox.send_keys(Keys.ENTER)
        time.sleep(1)
        self.wait_for_row_in_list_table(str(number) + ': ' + text)


    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get(self.live_server_url)

        # Untuk mengecek title dan h1
        self.assertIn('Anugrah Iman', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Anugrah Iman To-Do', header_text)

        # Untuk menggunakan baris 
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )

        table = self.browser.find_element_by_id('id_list_table')
        # If there are no comment
        if(len(table.find_elements_by_tag_name('tr')) == 0) :
            self.check_for_comment("yey, waktunya berlibur")
            # Input something
            self.input_something_to_list(1, 'Tugas membuat down')
            # Check the comment
            self.check_for_comment("sibuk tapi santai")
            self.input_something_to_list(2, 'Down memudahkan terkena corona')
            self.input_something_to_list(3, 'Tugas terosssss')
            self.input_something_to_list(4, 'Selesaiii')
            self.input_something_to_list(5, 'Muncul tugas lagi')
            # Check the comment
            self.check_for_comment("oh tidak")
        # If there are already comments below five
        elif(0 < len(table.find_elements_by_tag_name('tr')) < 5) :
            self.check_for_comment("sibuk tapi santai")
        # If there are already more than five comments
        elif(len(table.find_elements_by_tag_name('tr')) > 5) :
            self.check_for_comment("oh tidak")

        
        # Cek
        self.fail('Finish the test!')

        # The page updates again, and now shows both items on her list

# if __name__ == '__main__':
#     unittest.main(warnings='ignore')